import tensorflow as tf
import numpy as np
import gzip


DIR  = '../../RAW_DATA/MNIST/'

filename = [
["training_images","train-images-idx3-ubyte.gz"],
["test_images","t10k-images-idx3-ubyte.gz"],
["training_labels","train-labels-idx1-ubyte.gz"],
["test_labels","t10k-labels-idx1-ubyte.gz"]
]

def load_mnist():
    mnist = {}
    for name in filename[:2]:
        with gzip.open(DIR + name[1], 'rb') as f:
            mnist[name[0]] = np.frombuffer(f.read(), np.uint8, offset=16).reshape(-1,28*28)
    for name in filename[-2:]:
        with gzip.open(DIR + name[1], 'rb') as f:
            mnist[name[0]] = np.frombuffer(f.read(), np.uint8, offset=8)
    return mnist

def save_mnist(mnist):
    train_imgs = mnist["training_images"]
    train_labels = mnist["training_labels"]
    save_to_tfrecord('mnist_train.tfrecords', train_imgs, train_labels)

    test_imgs = mnist["test_images"]
    test_labels = mnist["test_labels"]
    save_to_tfrecord('mnist_test.tfrecords', test_imgs, test_labels)
    print('save completed')

def int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
def bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def save_to_tfrecord(train_filename, images, labels):
    # open the TFRecords file
    writer = tf.python_io.TFRecordWriter(train_filename)
    for index in range(len(labels)):
        #print(np.shape(images[index]))
        image_raw = images[index]
        image_raw = image_raw.tostring()
        # Create a feature
        feature = {'label': int64_feature(int(labels[index])),
                   'image': bytes_feature(image_raw)}

        # Create an example protocol buffer
        example = tf.train.Example(features=tf.train.Features(feature=feature))

        # Serialize to string and write on the file
        writer.write(example.SerializeToString())
    writer.close()


class MNIST:
    def __init__(self, sess, train_batch_size, test_batch_size=None):
        # load and preprocess data
        filenames = ["dataset/mnist_train.tfrecords"]
        dataset = tf.data.TFRecordDataset(filenames)

        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                      "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]
          label = parsed_features["label"]
          label = tf.one_hot(label, 10)

          image = tf.decode_raw(image_raw, tf.uint8)
          image = tf.reshape(image, [28, 28, 1])
          image = tf.cast(image, tf.float32)/255.
          #image = 2.0 * image - 1.0 # scale to -1 to 1

          return image, label

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=10000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images, next_labels = iterator.get_next()
        self.next_train_images = next_images
        self.next_train_labels = next_labels

        if test_batch_size is not None:
            test_filename = ["dataset/mnist_test.tfrecords"]
            test_dataset = tf.data.TFRecordDataset(test_filename)
            test_dataset = test_dataset.map(_parse_function)
            test_dataset = test_dataset.repeat()
            test_dataset = test_dataset.shuffle(buffer_size=10000)
            test_dataset = test_dataset.batch(test_batch_size)
            test_iterator = test_dataset.make_one_shot_iterator()
            next_test_images, next_test_labels = test_iterator.get_next()
            self.next_test_images = next_test_images
            self.next_test_labels = next_test_labels

if __name__ == "__main__":
    # load from gz file and save to tfrecords
    mnist = load_mnist()
    save_mnist(mnist)
