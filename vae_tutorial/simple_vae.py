import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from dataset.mnist import *
from utils import fc, flatten, gaussian_log_likelihood


class VAE:
    def __init__(self, sess, x):
        self.sess = sess

        """
        Model and Loss
        """
        x = flatten(x)
        # build autoencoder
        z_mean, z_sig = self._build_encoder(x)
        eps = tf.random_normal(tf.shape(z_sig))
        z_sample = z_mean + eps * z_sig
        x_recon = self._build_decoder(z_sample)

        # Loss functions
        kl_loss = tf.reduce_mean(
                    - gaussian_log_likelihood(z_sample, 0.0, 1.0, eps=0.0) \
                    + gaussian_log_likelihood(z_sample, z_mean, z_sig)
                  )

        recon_loss = tf.reduce_mean(
                    tf.reduce_sum(
                        tf.nn.sigmoid_cross_entropy_with_logits(logits=x_recon , labels=x)
                    ,axis=1))

        total_loss = kl_loss + recon_loss
        optimizer = tf.train.AdamOptimizer(0.0001)
        self.train_op = optimizer.minimize(total_loss)

        """
        Summary
        """
        self.summary = tf.summary.merge([
            tf.summary.scalar("train/recon_loss", recon_loss),
            tf.summary.scalar("train/kl_loss", kl_loss),
            tf.summary.image("x", tf.reshape(x, [-1, 28, 28, 1]) ),
            tf.summary.image("x_recon", tf.reshape( tf.nn.sigmoid(x_recon),[-1, 28, 28, 1] ))
        ])



    def _build_encoder(self, x):
        with tf.variable_scope('encoder'):
            h1 = tf.nn.relu( fc(x, 'h1', 1000) )
            h2 = tf.nn.relu( fc(h1, 'h2', 1000))
            z_mean = fc(h2, 'z_mean', 2)
            z_sig  = tf.nn.softplus( fc(h2, 'z_sig', 2))
        return z_mean, z_sig

    def _build_decoder(self, z):
        with tf.variable_scope('decoder'):
            h1 = tf.nn.relu(fc(z, 'h1', 1000))
            h2 = tf.nn.relu(fc(h1, 'h2', 1000))
            x_recon = fc(h2, 'x_out', 784) # 28*28
        return x_recon

    def train(self):
        return self.sess.run([self.summary, self.train_op])

    def _visualise_latent_space(self):
        # draw latent space
        
        return




if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--logdir', help='tb directory', default='experiments/')
    parser.add_argument('--exp_name', default='test')
    args = parser.parse_args()

    summary_dir = args.logdir + args.exp_name
    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    with tf.Session(config=config) as sess:
        summary_writer = tf.summary.FileWriter(summary_dir)
        data = MNIST(sess, train_batch_size=200)
        x = data.next_train_images
        net = VAE(sess, x)
        sess.run(tf.global_variables_initializer())

        for i in range(1000000):
            summary, _ = net.train()
            summary_writer.add_summary(summary, i)
            summary_writer.flush()
