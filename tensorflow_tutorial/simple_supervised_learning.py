import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


class model:
    def __init__(self):
        self.x = x = tf.placeholder(dtype=tf.float32, shape=[None], name='input')

        W = tf.get_variable("w1", [1], initializer=tf.initializers.truncated_normal)
        B = tf.get_variable("b1", [1], initializer=tf.initializers.truncated_normal)

        self.y_pred = y = W * x ** 3 + B



        self.y_target = y_target = tf.placeholder(dtype=tf.float32, shape=[None], name='target')

        self.loss = loss =  tf.reduce_sum(tf.square(y - y_target))

        optimizer = tf.train.AdamOptimizer(0.1)
        self.train_op = optimizer.minimize(loss)


if __name__ == "__main__":

    # Create dataset
    eps = np.random.rand(100)
    x = np.array([i for i in range(100)])
    y = 0.1 * x ** 3  - 0.3 + eps

    # Create model
    net = model()

    # Create Session
    with tf.Session() as sess:
        # Initialise variable
        sess.run(tf.global_variables_initializer())

        for i in range(10000):
            loss, _ = sess.run([net.loss, net.train_op], feed_dict={net.x:x, net.y_target:y})
            print(loss)

        y_pred = sess.run(net.y_pred, feed_dict={net.x:x})

        plt.scatter(x, y)
        plt.scatter(x, y_pred)
        plt.show()
